var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({  
  userId: String,
  emailId: String,
  password: String
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');